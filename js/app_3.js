let number = +prompt("Number");
if (number < 5) {
  console.log("Sorry, no numbers");
}
while (!Number.isInteger(number)) {
  number = +prompt("Number!");
}

for (let i = 5; i <= number; i++) {
  if (i % 5 !== 0) {
    continue;
  }
  console.log(i);
}
